//Write a program to find the volume of a tromboloid using one function
#include<stdio.h>
int main()
{
	float h,b,d,vol=0;
	printf("Enter height, breadth and depth of tromboloid: \n");
	scanf("%f%f%f",&h,&b,&d);
	vol = (1.0/3.0)*((h*b*d)+(d/b));
	printf("The volume of the tromboloid with h=%f, b=%f, d=%f is %f",h,b,d,vol);
	return 0;
}